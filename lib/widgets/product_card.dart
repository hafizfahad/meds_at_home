import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:meds_at_home/screens/product_detail.dart';

class ProductCard extends StatelessWidget {
  final DocumentSnapshot product;
  final DocumentSnapshot user;

  ProductCard({this.product,this.user});

  @override
  Widget build(BuildContext context) {
        return SingleChildScrollView(
          child: Container(
            child: GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (_)=> ProductDetails(product: product,user: user,)));
              },
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(30))
                ),
                color:  Color(0xff252525),
                child: Row(
                  children: <Widget>[
//                     Padding(
//                      padding: const EdgeInsets.all(8.0),
//                      child: ClipRRect(
//                        borderRadius: BorderRadius.circular(10),
//                        child: Image.network(
//                          product.data['picture'],
//                          height: 80,
//                          width: 70,
//                          fit: BoxFit.cover,
//                        ),
//                      ),
//                    ),

                    SizedBox(width: 10,),

                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: RichText(text: TextSpan(
                          children: [
                            TextSpan(text: '${product.data['name']} \n', style: TextStyle(fontSize: 20),),
//                    TextSpan(text: 'by: ${product.data['category']} \n', style: TextStyle(fontSize: 16, color: Colors.grey),),
                            TextSpan(text: 'Rs. ${product.data['unit_price']} \t', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),),
//                    TextSpan(text: 'ON SALE ' ,style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400, color: Colors.red),),
                          ], style: TextStyle(color: Colors.white)
                      ),),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
  }
}
