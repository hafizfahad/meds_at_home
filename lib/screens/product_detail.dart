import 'dart:ui';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meds_at_home/provider/user_provider.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:provider/provider.dart';
import 'cart.dart';
import 'package:meds_at_home/provider/cart_provider.dart';

import 'cart_page.dart';
import 'login.dart';

class ProductDetails extends StatefulWidget {
  final DocumentSnapshot product;
  final DocumentSnapshot user;

  ProductDetails({this.product,this.user});
  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {

  bool _pressed = true;
  CartService _cartService = CartService();

  int _currentValue = 1;
  int _result = 0;
  int _radioValue=0;
  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          _result = 0;
        break;
        case 1:
          _result = 1;
        break;
        case 2:
          _result = 2;
        break;
      }
      });
  }

  @override
  Widget build(BuildContext context) {
    final users = Provider.of<UserProvider>(context);

    return Scaffold(
      appBar: new AppBar(
        elevation: 0.0,
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Color(0xff008db9)),
        leading: InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Icon(Icons.arrow_back_ios)),
//        title: Text(widget.product.data['name'],
//          style: TextStyle(
//              fontWeight: FontWeight.w700,
//              fontSize: 18.0,
//              color: Colors.black54,
//              fontFamily: "Gotik"),),
        actions: <Widget>[
//          if(users.status == Status.Authenticated)
//            new IconButton(
//                icon: Icon(Icons.favorite, color: Color(0xFF6991C7),), onPressed: () {}),
//          new IconButton(icon: Icon(Icons.shopping_cart, color: Color(0xFF6991C7),),
//              onPressed: () {
////                Navigator.push(context, MaterialPageRoute(builder: (context) => new Cart()));
//              }),
        ],
      ),
      body: SafeArea(child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: 100,
                    child: Center(
                      child: Text(widget.product.data['name'],
                        style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),),
//                    Image.network(
//                      widget.product.data['picture'],
//                      height: 250,
////                  fit: BoxFit.cover,
//                    ),
                    ),
                  ),
                ],
              ),
              Divider(color: Colors.blueGrey,),
                Container(
                  child: Column(
                    children: <Widget>[
//                      if(widget.product.data['category'] == 'inculine')
//                        Padding(
//                          padding: const EdgeInsets.all(1.0),
//                          child: Row(
//                            mainAxisAlignment: MainAxisAlignment.center,
//                            children: <Widget>[
//                              new Radio(
//                                value: 0,
//                                groupValue: _radioValue,
//                                onChanged: _handleRadioValueChange,
//                              ),
//                              new Text(
//                                'Per Inculine',
//                                style: new TextStyle(fontSize: 16.0),
//                              ),
//                              new Radio(
//                                value: 2,
//                                groupValue: _radioValue,
//                                onChanged: _handleRadioValueChange,
//                              ),
//                              new Text(
//                                'Per Pack',
//                                style: new TextStyle(
//                                  fontSize: 16.0,
//                                ),
//                              ),
//                            ],
//                          ),
//                        ),
//                      if(widget.product.data['category'] == 'tablet')
//                        Padding(
//                          padding: const EdgeInsets.all(1.0),
//                          child: Row(
//                            mainAxisAlignment: MainAxisAlignment.center,
//                            children: <Widget>[
//                              new Radio(
//                                value: 0,
//                                groupValue: _radioValue,
//                                onChanged: _handleRadioValueChange,
//                              ),
//                              new Text(
//                                'Per Tablet',
//                                style: new TextStyle(fontSize: 16.0),
//                              ),
////                            new Radio(
////                                value: 1,
////                                groupValue: _radioValue,
////                                onChanged: _handleRadioValueChange,
////                              ),
////                              new Text(
////                                'Per Strip',
////                                style: new TextStyle(
////                                  fontSize: 16.0,
////                                ),
////                              ),
//                              new Radio(
//                                value: 2,
//                                groupValue: _radioValue,
//                                onChanged: _handleRadioValueChange,
//                              ),
//                              new Text(
//                                'Per Pack',
//                                style: new TextStyle(fontSize: 16.0),
//                              ),
//                            ],
//                          ),
//                        ),
//                      if(widget.product.data['category'] == 'capsul')
                        Padding(
                          padding: const EdgeInsets.all(1.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Radio(
                                value: 0,
                                groupValue: _radioValue,
                                onChanged: _handleRadioValueChange,
                              ),
                              new Text(
                                'Per Unit',
                                style: new TextStyle(fontSize: 16.0),
                              ),
//                              new Radio(
//                                value: 1,
//                                groupValue: _radioValue,
//                                onChanged: _handleRadioValueChange,
//                              ),
//                              new Text(
//                                'Per Strip',
//                                style: new TextStyle(
//                                  fontSize: 16.0,
//                                ),
//                              ),
                              new Radio(
                                value: 2,
                                groupValue: _radioValue,
                                onChanged: _handleRadioValueChange,
                              ),
                              new Text(
                                'Per Pack',
                                style: new TextStyle(fontSize: 16.0),
                              ),
                            ],
                          ),
                        ),
                      Divider(color: Colors.blueGrey,),
//                        Container(
//                          child: Row(
//                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                            children: <Widget>[
//                              new Text("Quantity: $_currentValue",style: TextStyle(fontSize: 20),),
//                              new NumberPicker.integer(
//                                  initialValue: _currentValue,
//                                  minValue: 1,
//                                  maxValue: 30,
//                                  onChanged: (newValue) =>
//                                      setState(() => _currentValue = newValue)),
//
//                            ],
//                          ),
//                        ),
                    Container(
                      height: MediaQuery.of(context).size.height*.23,
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Quantity: ",
                              style: TextStyle(fontSize: 20),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                InkWell(
                                  onTap: (){
                                    setState(() {
                                      _currentValue += 1;
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.blue,
                                      ),
                                      borderRadius: BorderRadius.circular(5.0),
                                    ),
                                    child: Icon(Icons.add,color: Colors.blue,)
                                  ),
                                ),
                                Divider(),
                                Text("$_currentValue",
                                  style: TextStyle(fontSize: 20),),
                                Divider(),
                                InkWell(
                                  onTap: (){
                                    setState(() {
                                      if(_currentValue > 0){
                                        _currentValue -= 1;
                                      }
                                    });
                                  },
                                  child: Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Colors.blue,
                                        ),
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      child: Icon(Icons.remove,color: Colors.blue,)
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )
                      ),
                    ),
                      Divider(color: Colors.blueGrey,),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.all(9.0),
                                      child: RichText(text: TextSpan(
                                        children:[
                                          if(_result == 0)
                                            TextSpan(
                                              text: 'Rs.${widget.product
                                                  .data['unit_price'] *
                                                  _currentValue}',
                                              style: TextStyle(fontSize: 20,
                                                  fontWeight: FontWeight
                                                      .w600),),
//                                          if(_result == 1)
//                                            TextSpan(
//                                              text: 'Rs.${widget.product
//                                                  .data['unit_price'] *
//                                                  _currentValue*widget.product.data['quantity_tablets_per_strip']}',
//                                              style: TextStyle(fontSize: 20,
//                                                  fontWeight: FontWeight
//                                                      .w600),),
                                          if(_result == 2)
                                              TextSpan(
                                                text: 'Rs.${widget.product.data['unit_price'] *
                                                    _currentValue*
                                                    widget.product.data['quantity_units_per_pack']}',
                                                style: TextStyle(fontSize: 20,
                                                    fontWeight: FontWeight
                                                        .w600),),
                                              TextSpan(
                                                  text: '\nprice/selected quantity')
                                            ],style: TextStyle(color: Colors.black),
                                      ),),
                                    ),
                                  ),
                                ],
                              ),
                              VerticalDivider(),
                              Column(
                                children: <Widget>[
//                                  if(widget.product.data['category'] == 'tablet')
//                                    Container(
//                                      child: Padding(
//                                          padding: const EdgeInsets.all(1.0),
//                                          child: RichText(text: TextSpan(
//                                            children: [
//                                              TextSpan(text: 'Price/Tablet: Rs.${widget.product.data['price_tablet']}'),
////                                              TextSpan(text: '\nTablets/strip: ${widget.product.data['quantity_tablets_per_strip']}'),
////                                              TextSpan(text: '\nStrips/pack: ${widget.product.data['quantity_strips_per_pack']}'),
//                                              TextSpan(text: '\nTablets/pack: ${widget.product.data['quantity_strips_per_pack']*
//                                                  widget.product.data['quantity_tablets_per_strip']}')
//                                            ],style: TextStyle(color: Colors.black),
//                                          ),)
//                                      ),
//                                    ),
//                                  if(widget.product.data['category'] == 'capsul')
//                                    Container(
//                                      child: Padding(
//                                          padding: const EdgeInsets.all(1.0),
//                                          child: RichText(text: TextSpan(
//                                            children: [
//                                              TextSpan(text: 'Price/Capsul: Rs.${widget.product.data['price_tablet']}'),
////                                              TextSpan(text: '\nCapsuls/strip: ${widget.product.data['quantity_tablets_per_strip']}'),
////                                              TextSpan(text: '\nStrips/pack: ${widget.product.data['quantity_strips_per_pack']}'),
//                                              TextSpan(text: '\nCapsuls/pack: ${widget.product.data['quantity_strips_per_pack']*
//                                                  widget.product.data['quantity_tablets_per_strip']}')
//                                            ],style: TextStyle(color: Colors.black),
//                                          ),)
//                                      ),
//                                    ),
//                                  if(widget.product.data['category'] == 'inculine')
                                    Container(
                                      child: Padding(
                                        padding: const EdgeInsets.all(1.0),
                                        child: RichText(text: TextSpan(
                                          children: [
                                            TextSpan(text: 'Unit Price: Rs.${widget.product.data['unit_price']}'),
                                            TextSpan(text: '\nUnits/pack: ${widget.product.data['quantity_units_per_pack']}'),
                                          ],style: TextStyle(color: Colors.black),
                                        ),)
                                      ),
                                    )
                                ],
                              ),
                            ],
                          ),
                        ),
                      Divider(color: Colors.blueGrey,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: RichText(text: TextSpan(
                                  children: [
                                    TextSpan(text: 'Primary use:',style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                                    TextSpan(text: '\n${widget.product.data['description']}', style: TextStyle(color: Colors.black)),
                                  ],
                                ),)
                              ),
                            ],
                          ),
                        ],
                      ),
                      Divider(),
                    ],
                  ),
                ),
            ],
          ),
        ),
      )
      ),
        bottomNavigationBar:BottomAppBar(
//          color: Color(0xFF6991C7),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
//              if(users.status == Status.Authenticated)
                Padding(
                padding: const EdgeInsets.all(08.0),
                child: InkWell(
                  onTap: () {
                    if(_pressed){
                      _cartService.createCart(
                            widget.user,
                            widget.product,
                            _result,
                            _currentValue);
                      this.setState((){
                        _pressed = false;
                      });
//                      _pressed = false;
                    }
                    },
                  child: Container(
                    height: 55.0,
                    width: 150.0,
                    decoration: BoxDecoration(
                        color: Color(0xff008db9),
                        borderRadius: BorderRadius.all(Radius.circular(40.0))),
                    child: Center(
                      child: Text(_pressed?
                        "Add to Cart":"Added",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 16.5,
                            letterSpacing: 0.0),
                      ),
                    ),
                  ),
                ),
              ),
//              if(users.status == Status.Authenticated)
                Padding(
                padding: const EdgeInsets.all(8.0),
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                      context, MaterialPageRoute(
                      builder: (_) =>
                      CartPage(
                        user: widget.user,
                        product: widget.product,
                      )));
                    },
                  child: Container(
                    height: 55.0,
                    width: 150.0,
                    decoration: BoxDecoration(
                        color: Color(0xff008db9),
                        borderRadius: BorderRadius.all(Radius.circular(40.0))),
                    child: Center(
                      child: Text(
                        "Go to Cart",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 16.5,
                            letterSpacing: 0.0),
                      ),
                    ),
                  ),
                ),
              ),
//              if(users.status != Status.Authenticated)
//                Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: InkWell(
//                    onTap: () {
//                      Navigator.push(
//                          context, MaterialPageRoute(
//                          builder: (_) =>
//                              Login()));
//                    },
//                    child: Container(
//                      height: 55.0,
//                      width: 300.0,
//                      decoration: BoxDecoration(
//                          color: Colors.indigoAccent,
//                          borderRadius: BorderRadius.all(Radius.circular(40.0))),
//                      child: Center(
//                        child: Text(
//                          "Login to Shop",
//                          style: TextStyle(
//                              color: Colors.white,
//                              fontWeight: FontWeight.w700,
//                              fontSize: 16.5,
//                              letterSpacing: 1.0),
//                        ),
//                      ),
//                    ),
//                  ),
//                ),
            ],
          ),
        ),
    );
  }
}