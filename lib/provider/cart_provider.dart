import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:uuid/uuid.dart';


class CartService{
  Firestore _firestore = Firestore.instance;
  String ref = 'cart';

  int k = 0;
  List<String> cartProduct = [];
  void createCart(DocumentSnapshot user, DocumentSnapshot cart, int type, int quantity){

    var id = Uuid();
    String cartId = id.v1();
    cartProduct.add(cart.data['name']);
  if(type == 0)
      _firestore.collection(ref).document(cartId).setData({
      'user_name': user.data['name'],
      'user_id': user.data['uid'],
      'id': cartId,
      'product_name': cart.data['name'],
      'quantity': quantity,
      'selected_type': 'Unit',
      'total_price': cart.data['unit_price']*quantity,
      'unit_price': cart.data['unit_price'],
      'prescription_required': cart.data['prescription_required'],
//      "quantity_tablets_per_strip": cart.data["quantity_tablets_per_strip"],
      "quantity_units_per_pack": cart.data["quantity_units_per_pack"],
    });
//  if(type == 1)
//    _firestore.collection(ref).document(cartId).setData({
//      'user_name': user.data['name'],
//      'user_id': user.data['uid'],
//      'id': cartId,
//      'product_name': cart.data['name'],
//      'quantity':quantity,
//      'selected_type': 'Strip',
//      'total_price': cart.data['price_tablet']*cart.data['quantity_tablets_per_strip']*quantity,
//      'unit_price': cart.data['price_tablet'],
//      "quantity_tablets_per_strip": cart.data["quantity_tablets_per_strip"],
//      "quantity_strips_per_pack": cart.data["quantity_strips_per_pack"],
//    });
  if(type == 2)
    _firestore.collection(ref).document(cartId).setData({
      'user_name': user.data['name'],
      'user_id': user.data['uid'],
      'id': cartId,
      'product_name': cart.data['name'],
      'quantity':quantity,
      'selected_type': 'Pack',
      'total_price': cart.data['unit_price']*cart.data['quantity_units_per_pack']*quantity,
      'unit_price': cart.data['unit_price'],
      'prescription_required': cart.data['prescription_required'],
//      "quantity_tablets_per_strip": cart.data["quantity_tablets_per_strip"],
      "quantity_units_per_pack": cart.data["quantity_units_per_pack"],
    });
  }
}